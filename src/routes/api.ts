import { Hono } from "hono";
import { HonoContext } from "..";

const app = new Hono<HonoContext>()

app.get('/session', async _ => {
    return _.json(_.get('session'));
})

app.get('/echo', async _ => {
    return _.json(_.req.query());
})

app.use('/*', async _ => {
    return _.json({
        error: 404,
        message: "Not Found"
    })
})

export default app;