import { UUID } from "crypto";

export default interface User {
    _id: UUID
    login: string
    firstname: string
    lastname: string
}