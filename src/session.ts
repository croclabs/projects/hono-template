import { UUID } from "crypto";

export default interface Session {
    id: UUID
    user?: UUID
}