import chalk, { ChalkInstance } from "chalk";
import fs, { WriteStream } from 'fs';
import { HonoRequest } from "hono";

let today = new Date().toLocaleString('se').split(' ')[0];
let logPrefix = `${process.env.LOG_PREFIX}/${today}`;

if (!fs.existsSync(logPrefix)) {
    fs.mkdirSync(logPrefix, {recursive: true})
}

let serverStream = fs.createWriteStream(`${logPrefix}/server.log`, {flags: 'a'});

let debugStream = fs.createWriteStream(`${logPrefix}/debug.log`, {flags: 'a'});
let infoStream = fs.createWriteStream(`${logPrefix}/info.log`, {flags: 'a'});
let warningStream = fs.createWriteStream(`${logPrefix}/warning.log`, {flags: 'a'});
let errorStream = fs.createWriteStream(`${logPrefix}/error.log`, {flags: 'a'});
let requestStream = fs.createWriteStream(`${logPrefix}/request.log`, {flags: 'a'});

let streams = new Map<string, WriteStream>;

streams.set('DEBUG', debugStream);
streams.set('INFO', infoStream);
streams.set('WARNING', warningStream);
streams.set('ERROR', errorStream);
streams.set('REQUEST', requestStream);
streams.set('SERVER', serverStream);

export function debug(msg: string | undefined) {
    log('DEBUG  ', msg, new Date(), chalk.blueBright);
}

export function warn(msg: string | undefined) {
    log('WARNING', msg, new Date(), chalk.yellow);
}

export function info(msg: string | undefined) {
   log('INFO   ', msg, new Date(), chalk.greenBright);
}

export function err(msg: string | undefined) {
    log('ERROR  ', msg, new Date(), chalk.red);
}

export function request(req: HonoRequest) {
    log('REQUEST', `${req.method} ${req.path}\n\tquery: ${JSON.stringify(req.query())}\n\tparams: ${JSON.stringify(req.param())}`, new Date(), chalk.gray);
}

function log(level: string, text: string | undefined, date: Date, chalkFct?: ChalkInstance) {
    let caller = new Error().stack!.split('at ')[3].replace(')', '').replace('\n', '').split('\\');
    let callerStr = caller[caller.length - 1];

    let colored = process.env.LOG_TEMPLATE
        ?.replaceAll('%l', chalkFct == null ? level : chalkFct(level))
        ?.replaceAll('%c', callerStr.trim())
        ?.replaceAll('%d', date.toLocaleString('se'))
        ?.replaceAll('%m', text ?? 'null')
    console.log(colored);

    let fileStr = process.env.LOG_TEMPLATE
        ?.replaceAll('%l', level)
        ?.replaceAll('%c', callerStr.trim())
        ?.replaceAll('%d', date.toLocaleString('se'))
        ?.replaceAll('%m', text ?? 'null') + '\n'
    streams.get(level.trim())?.write(fileStr);
    streams.get('SERVER')?.write(fileStr);
}