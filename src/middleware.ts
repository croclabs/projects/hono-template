import { serveStatic } from '@hono/node-server/serve-static';
import { randomUUID } from 'crypto';
import { getSignedCookie, setCookie, setSignedCookie } from 'hono/cookie';
import { createFactory } from 'hono/factory';
import { HonoContext } from '.';
import { request } from './logger';
import Session from './session';

let secret = process.env.COOKIE_SECRET ?? 'undefined';

const factory = createFactory<HonoContext>()

const session = factory.createMiddleware(async (_, next) =>{
    let sesCookie = await getSignedCookie(_, secret, 'session');
    let session: Session;

    if (sesCookie != null && typeof sesCookie === 'string') {
        session = JSON.parse(sesCookie) as Session;
    } else {
        session = {
            id: randomUUID(),
            user: undefined
        }
    }

    await setSignedCookie(_, 'session', JSON.stringify(session), secret, {
        secure: true,
        maxAge: 1000,
        httpOnly: true,
        sameSite: 'lax'
    })

    _.set('session', session);
    await next();
})

const lastPath = factory.createMiddleware(async (_, next) => {
    if (_.req.header('Sec-Fetch-Mode') === 'navigate') {
        setCookie(_, 'last_path', _.req.path, {
            sameSite: 'strict',
            secure: true,
            httpOnly: true,
        })
    }

    await next();
})

const requestLogger = factory.createMiddleware(async (_, next) => {
    request(_.req)
    await next();
})

const serveFiles = serveStatic({root: './static/', rewriteRequestPath: path => {
    if (!path.includes('.')) {
        return '/index.html'
    } else {
        return path;
    }
}})

export {
    serveFiles,
    session,
    lastPath,
    requestLogger,
};

