import 'dotenv/config'
import { serve } from '@hono/node-server'
import { Hono } from 'hono'
import { debug } from './logger'
import { lastPath, requestLogger, serveFiles, session } from './middleware'
import api from './routes/api'
import Session from './session'

export type Variables = {
    session: Session
}

export interface HonoContext {
    Variables: Variables
}

const app = new Hono<HonoContext>()

app.use(requestLogger)
app.use(session)
app.use(lastPath)

app.route('/api', api)

app.use(serveFiles)

const port = 3000
debug(`Server is running on port ${port}`)

serve({
    fetch: app.fetch,
    port
})
